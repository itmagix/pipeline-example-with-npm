import hudson.model.*

// Example Jenkinsfile by Maikel Dollé

// Docker image with NPM installed
String DOCKER_NPM_URL = "maikeldolle/maven-jdk8-npm"

// Connect to Docker daemon on host to enable container creation on the host from within containers started with this argument
String DOCKER_NPM_ARGS = "-v /var/run/docker.sock:/var/run/docker.sock -u jenkins:jenkins"

String SUCCESS_EMAIL_TEXT = "is back to normal"
String FAILURE_EMAIL_TEXT = "failed"
String UNSTABLE_EMAIL_TEXT = "is unstable"

// Since each stage gets a new copy of the Jenkins workspace, we'll need to install the node_modules multiple times.
def prepareNodeModules() {
    echo '--- Configure NPM'
    sh "npm config set strict-ssl false"
    echo 'sleep 1'
    sh "sleep 5"
    
    echo '--- Allowing unsafe perms as work-around'
    sh "npm config set unsafe-perm=true"
    echo 'sleep 1,5'
    sh "sleep 5"
    
    echo '--- Installing Global Dependencies'
    sh "npm install -g grunt --verbose"
    echo 'sleep 2'
    sh "sleep 5"
    
    echo '--- Installing Local Dependencies'
    sh "npm install --verbose"
    echo 'sleep 3'
    sh "sleep 5... en doooor"
}

pipeline {
    agent { label 'slave' }
    
    stages {
        stage('Build') {
            steps {
                // CD into frontend directory. All commands will be executed from here.
                // (withDockerContainer must be nested within this directive: https://issues.jenkins-ci.org/browse/JENKINS-46636)
                dir('add-child') {
                    // Execute the containing commands in the NPM docker container
                    withDockerContainer(image: DOCKER_NPM_URL, args: DOCKER_NPM_ARGS) {
                        prepareNodeModules()
                        sh "grunt --help"
                    }    
                }
            }
        }
        
        stage('Unit Tests') {
            steps {
                dir('add-child') {
                    withDockerContainer(image: DOCKER_NPM_URL, args: DOCKER_NPM_ARGS) {
                        prepareNodeModules()
                        // Run Karma with singleRun configuration (no autoWatch)
                        sh "grunt karma:ci"
                    }
                }
            }
        }
    }
}